//                                    Version 1.105
//Changes:  1.Replaced servo with 3 Ultrasonic Sensors (180 Degree View)
//          2.simplified code ----to be done




#include <NewPing.h>      //add Ultrasonic sensor library

#define TRIG_PIN A0 // Pin A0 on the Motor Drive Shield soldered to the ultrasonic sensor
#define ECHO_PIN_0 A1 // Pin A1 on the Motor Drive Shield soldered to the ultrasonic sensor
#define ECHO_PIN_1 A2
#define ECHO_PIN_2 A3
#define MAX_DISTANCE 300 // sets maximum useable sensor measuring distance to 300cm


#define COLL_DIST 30 // sets distance at which robot stops and reverses to 30cm


NewPing sonarCenter(TRIG_PIN, ECHO_PIN_0, MAX_DISTANCE); // sets up sensor library to use the correct pins to measure distance.
NewPing sonarLeft(TRIG_PIN, ECHO_PIN_1, MAX_DISTANCE);
NewPing sonarRight(TRIG_PIN, ECHO_PIN_2, MAX_DISTANCE);


const int motorA1= 6;		  //motor A positive (+) pin to pin 6 (PWM) (from L298 module!)
const int motorA2= 9;     //motor A negative (-) pin to pin 9 (PWM)
const int motorB1=10;		  //motor B positive (+) pin to pin 10 (PWM)
const int motorB2=11;		  //motor B negative (-) pin to pin 11 (PWM)




int frontDist = 10;
int rightDistance = 10;
int leftDistance = 10;


//-------------------------------------------- SETUP LOOP ----------------------------------------------------------------------------
void setup() {
 
  pinMode(motorA1,OUTPUT);
	pinMode(motorA2,OUTPUT);
	pinMode(motorB1,OUTPUT);
	pinMode(motorB2,OUTPUT);
  pinMode(TRIG_PIN,OUTPUT);
	pinMode(ECHO_PIN_0,INPUT);
	pinMode(ECHO_PIN_1,INPUT);
	pinMode(ECHO_PIN_2,INPUT);
	
  Serial.begin(9600);
 }
//------------------------------------------------------------------------------------------------------------------------------------

//---------------------------------------------MAIN LOOP ------------------------------------------------------------------------------
void loop() {
  
  readData();//get data from all the source
  
  action();
}

void readData()
{
  ultrasonicF();
  
  ultrasonicLF();
  
  ultrasonicRF();
  
}

void action()
{

  if (frontDist < COLL_DIST && frontDist > 0 ) 
  {
    changePath();
    
  }  // if forward is blocked change direction
  
  moveForward(); 
   
  if(rightDistance > 0 && rightDistance < 10 && leftDistance > 15 )
  {
    turnLeft();
  }
  if(leftDistance > 0 && leftDistance < 10  && rightDistance > 15)
  {
   turnRight();
  }
  
}

//----------------------------------------------------------Change Path----------------------------------------------------------------


void changePath() 
{
  moveStop();
  
  readData();
  
  compareDistance();
}

  
void compareDistance()   // find the longest distance
{
  if (leftDistance>rightDistance && leftDistance>COLL_DIST) //if left is less obstructed 
  {
    turnLeft();
    Serial.println("Turn Left ");
  }
  else if (rightDistance>leftDistance && rightDistance>COLL_DIST) //if right is less obstructed
  {
    turnRight();
     Serial.println("Turn Right ");
  }
   else //if they are equally obstructed
  {
    moveBackward();
     Serial.println("Back ");
  }
}


//-------------------------------------------------------------------------------------------------------------------------------------
void moveStop() {
  
  digitalWrite(motorA1, LOW);
	digitalWrite(motorA2, LOW);
	digitalWrite(motorB1, LOW);
	digitalWrite(motorB2, LOW);
  
}  // stop the motors.
//-------------------------------------------------------------------------------------------------------------------------------------
void moveForward() {
   
  digitalWrite(motorA1, HIGH);
	digitalWrite(motorA2, LOW);
	digitalWrite(motorB1, HIGH);
	digitalWrite(motorB2, LOW);	
}
//-------------------------------------------------------------------------------------------------------------------------------------
void moveBackward() {
    
  digitalWrite(motorA1, LOW);
	digitalWrite(motorA2, HIGH);
	digitalWrite(motorB1, LOW);
	digitalWrite(motorB2, HIGH);
	delay(500);
	changePath();
}  
//-------------------------------------------------------------------------------------------------------------------------------------
void turnRight() {
  
  digitalWrite(motorA1, LOW);
	digitalWrite(motorA2, HIGH);
	digitalWrite(motorB1, HIGH);
	digitalWrite(motorB2, LOW);	
	delay(1000);
	moveForward();

	
}  
//-------------------------------------------------------------------------------------------------------------------------------------
void turnLeft() {
 
  digitalWrite(motorA1, HIGH);
	digitalWrite(motorA2, LOW);
	digitalWrite(motorB1, LOW);
	digitalWrite(motorB2, HIGH);
	delay(1000);
	moveForward();

}  
//-------------------------------------------------------------------------------------------------------------------------------------
void ultrasonicF()
{
  
  
  frontDist = readPingCenter();   // read distance
  Serial.print("FrontDist: ");
  Serial.print(frontDist );
  
}

void ultrasonicLF()
{
  
  
  leftDistance = readPingLeft();   // read distance
  Serial.print("leftDistance: ");
  Serial.print(leftDistance );
}

void ultrasonicRF()
{
 
  
  rightDistance = readPingRight();   // read distance
  Serial.print("rightDistance: ");
  Serial.println(rightDistance );
}


//-------------------------------------------------------------------------------------------------------------------------------------

int readPingCenter() 
{ // read the ultrasonic sensor distance
  delay(70);   
  unsigned int uS = sonarCenter.ping();
  int cm = uS/US_ROUNDTRIP_CM;
  return cm;
}

int readPingLeft() 
{ // read the ultrasonic sensor distance
  delay(70);   
  unsigned int uS = sonarLeft.ping();
  int cm = uS/US_ROUNDTRIP_CM;
  return cm;
}

int readPingRight() 
{ // read the ultrasonic sensor distance
  delay(70);   
  unsigned int uS = sonarRight.ping();
  int cm = uS/US_ROUNDTRIP_CM;
  return cm;
}